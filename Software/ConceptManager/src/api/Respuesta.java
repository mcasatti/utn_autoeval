/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Modela una respuesta. Hereda de Elemento Examen.
 * @author Martin
 */
public class Respuesta extends ElementoExamen
{
    /** 
     * Peso de la respuesta, se calcula luego de evaluar la respuesta y se
     * puede comparar con otra respuesta.
     */
    private double Peso;
    /**
     * Utilizado para intercambios entre cliente y base de datos.
     * Sin uso por el momento
     */
    private int Secuencia;
    /**
     * Query que modela la forma en que se debe buscar esta respuesta en el 
     * grafo.
     */
    private String Query = "";
    
    /**
     * Constructor. Crea una Respuesta a partir de un texto
     * @param texto Texto de la Respuesta a crear.
     */
    public Respuesta (String texto)
    {
        super (texto);
        Peso = 0;
        Secuencia = 0;
    }

    /**
     * Constructor. Crea una respuesta vacía
     */
    public Respuesta ()
    {
        super ("");
        Peso = 0;
        Secuencia = 0;
    }

    /**
     * Devuelve el peso de una respuesta
     * @return Peso de la respuesta
     */
    public double getPeso()
    {
        return Peso;
    }

    /**
     * Devuelve el número de secuencia actual de la Respuesta
     * @return Número de secuencia actual
     */
    public int getSecuencia()
    {
        return Secuencia;
    }
    
    /**
     * Asigna el peso a la Respuesta actual
     * @param Peso Peso asignado
     */
    public void setPeso (double Peso)
    {
        this.Peso = Peso;
    }
    
    /**
     * Asigna el número de secuencia a la Respuesta actual
     * @param Secuencia Número de secuencia
     */
    public void setSecuencia (int Secuencia)
    {
        this.Secuencia = Secuencia;
    }
    
    /**
     * Asigna el query que representa la Respuesta
     * Este query se puede utilizar con el motor de base de datos OrientDB
     * para obtener los conceptos y relaciones que modelan la respuesta
     * @param query Query, con sintaxis OrientDB
     */
    public void setQuery (String query)
    {
        this.Query = query;
    }
    
    /**
     * Obtiene el query que representa la Respuesta
     * @return String con el query
     */
    public String getQuery ()
    {
        return this.Query;
    }

    @Override
    public String toString() {
        return getTexto();
    }
    
        
    public String toStringF()
    {
        StringBuilder bld = new StringBuilder();
        for (Termino t : this.Terminos)
        {
            bld.append(t.toString());
            if (bld.length() > 0)
                bld.append(",");
        }
        return bld.toString();
    }
    
    /**
     * Devuelve los términos de la Respuesta como un string formateado, con los 
     * términos separados por coma
     * @param full Indica si la información de los terminos es completa o resumida
     * @return String con los términos separados por coma.
     */
    public String toStringF (boolean full)
    {
        StringBuilder bld = new StringBuilder();
        for (Termino t : this.Terminos)
        {
            bld.append(t.toString(full));
            if (bld.length() > 0)
                bld.append(",");
        }
        return bld.toString();
    }
    
    /**
     * Devuelve los valores Delta de desplazamiento de cada uno de los términos
     * @return String con los Delta de cada término
     */
    public String toStringDelta ()
    {
        StringBuilder bld = new StringBuilder();
        for (Termino t : this.Terminos)
        {
            bld.append(t.toStringDelta());
            if (bld.length() > 0)
                bld.append(",");
        }
        return bld.toString();
    }

    /**
     * Devuelve la estructura de la Respuesta, en formato XML, compacto.
     * @return String con formato XML
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     * @throws TransformerException 
     */
    public String toXML() throws ParserConfigurationException, TransformerConfigurationException, TransformerException
    {
        return toXML(true);
    }
    
    /**
     * Devuelve la estructura de la Respuesta, en formato XML, compacto o extendido.
     * @param compact Indica si el formato debe ser compacto(true) o extendido(false)
     * @return String con formato XML
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     * @throws TransformerException 
     */
    public String toXML(boolean compact) throws ParserConfigurationException, TransformerConfigurationException, TransformerException
    {
        Document doc = getXML();

        DOMSource source = new DOMSource(doc);
        
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try 
        {
            transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            if (!compact)
            {
                transformer.setOutputProperty(OutputKeys.INDENT,"yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount","3");
            }
            StringWriter writer = new StringWriter();
            transformer.transform(source, new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } 
        catch (TransformerException e) 
        {
            e.printStackTrace();
        }
         
        return null;            
        /*
        Transformer transformer=TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,"yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount","3");
        DOMSource source = new DOMSource(doc);

        // Output to console for testing
        //StreamResult result = new StreamResult(System.out);
        //StreamResult result = new StreamResult(new StringWriter());
        StreamResult result = new StreamResult(new File("C:\\termino.xml",""));
        try
        {
            transformer.transform(source, result);
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        

        return ""; //result.getWriter().toString();
        */
    }
    
    Document getXML() throws ParserConfigurationException, TransformerException
    {
        /*
        <Respuesta
        xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
        xsi:noNamespaceSchemaLocation='RespuestaPacket.xsd'
        Secuencia="3"
        Peso="0"
        >
        <Termino>
        <Nombre></Nombre>
        <Vista></Vista>
        <Tipo></Tipo>
        <Accion></Accion>
        <ErrorAccion></ErrorAccion>
        <Peso></Peso>
        </Termino>
        </Respuesta>
         */
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(false);
        docFactory.setValidating(false);
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        Element respuesta = doc.createElement("Respuesta");
        respuesta.setAttribute("Secuencia", String.valueOf(this.Secuencia));
        respuesta.setAttribute("Peso", String.valueOf(this.Peso));
        doc.appendChild(respuesta);
        for (Termino t : this.Terminos)
        {
            Node n = doc.importNode(t.toNode(), true);
            respuesta.appendChild(n);
        }
        return doc;
    }
        
    /**
     * Dada una respuesta candidata, pasada por parámetro,
     * compara los terminos con esta respuesta y devuelve
     * la original con los valores Delta cargados en sus términos.
     * Los valores de Delta posible son:<br>
     * = 0 el termino no es un concepto o está en el mismo lugar en ambas<br>
     * {@literal < 0} el término aparece antes en la respuesta candidata<br>
     * {@literal > 0} el término aparece despues en la respuesta candidata<br>
     * El valor, mayor o menor que cero indica la cantidad de lugares
     * desplazado.
     * 
     * @param candidata Respuesta candidata
     * @return Respuesta con los valores Delta actualizados
     */
    public Respuesta getDeltas (Respuesta candidata)
    {
        Respuesta deltas = candidata;
        for (Termino t : candidata.getTerminos())
        {
            int pos_base = getTerminoIndexByName(t.getNombre());
            int pos_cand = candidata.getTerminoIndexByName(t.getNombre());

            // pos_base == 0 quiere decir que no se encontró el término
            // de la candidata en la respuesta base
            if (pos_base == 0)
                t.setDelta(null);
            else
                t.setDelta(pos_cand-pos_base);
        }
        return deltas; 
    }

}
