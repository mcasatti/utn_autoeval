/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conceptmanager;

import com.google.common.base.CharMatcher;

/**
 * Modela una relación entre dos conceptos
 * @author Martin
 */
public class Relacion 
{
    String Id;
    String Nombre;
    String Tipo;
    
    /**
     * Constructor. 
     * @param Id
     * @param Nombre 
     */
    public Relacion (String Id, String Nombre)
    {
        this.Id = Id;
        this.Nombre = Nombre;
        this.Tipo = null;
    }

    /**
     * Constructor.
     * @param Id
     * @param Nombre
     * @param Tipo 
     */
    public Relacion (String Id, String Nombre, String Tipo)
    {
        this.Id = Id;
        this.Nombre = Nombre;
        this.Tipo = Tipo;
    }

    /**
     * Obtiene el id de la relacion
     * @return 
     */
    public String getId()
    {
        return Id;
    }

    /**
     * Obtiene el Nombre de la relación
     * @return 
     */
    public String getNombre()
    {
        return Nombre;
    }

    /**
     * Obtiene el tipo de la relacion
     * @return 
     */
    public String getTipo()
    {
        return Tipo;
    }
    
    /**
     * Almacena el Id de la relacion
     * @param Id 
     */
    public void setId(String Id)
    {
        this.Id = Id;
    }

    /**
     * Almacena el Nombre de la relación
     * @param Nombre 
     */
    public void setNombre(String Nombre)
    {
        this.Nombre = Nombre;
    }

    /**
     * Almacena el tipo de la relación
     * @param Tipo 
     */
    public void setTipo(String Tipo)
    {
        this.Tipo = Tipo;
    }

}


