/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conceptmanager;

import com.tinkerpop.blueprints.Vertex;
import java.util.Date;

/**
 *
 * @author Martin
 */
public class Concepto 
{
    String Nombre;
    String Id;
    
    /* TODO: Implementar el manejo de estas dos propiedades */
    String Usuario;
    Date Actualizado;
 
    public void setNombre(String Nombre)
    {
        this.Nombre = Nombre.toUpperCase();
    }

    public void setId(String Id)
    {
        this.Id = Id;
    }

    public void setUsuario(String Usuario)
    {
        this.Usuario = Usuario;
    }

    public void setActualizado(Date Actualizado)
    {
        this.Actualizado = Actualizado;
    }

    public String getNombre()
    {
        return Nombre;
    }

    public String getId()
    {
        return Id;
    }

    public String getUsuario()
    {
        return Usuario;
    }

    public Date getActualizado()
    {
        return Actualizado;
    }
        
    public Concepto ()
    {
        this.Id = null;
        this.Nombre = null;
    }
    
    public Concepto (String Id, String Nombre)
    {
        this.Id = Id;
        this.Nombre = Nombre.toUpperCase();
    }
}

